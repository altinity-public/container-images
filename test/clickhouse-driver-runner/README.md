# Running clickhouse-driver tests locally

clickhouse-driver: `https://github.com/mymarilyn/clickhouse-driver`

To run clickhouse-driver tests manually you need `clickhouse-common-static.deb` and the `clickhouse-client.deb` in your machine.

Then execute command

```bash
docker run --rm -v $(pwd)/PACKAGES:/clickhouse -e RELEASE=0.2.1 registry.gitlab.com/altinity-public/container-images/test/clickhouse-driver-runner:v1.0
```

Where `$(pwd)/PACKAGES` is the folder to the clickhouse*.deb packages and `RELEASE=0.2.1` is the clickhouse-driver version you want to test

Make sure you have access to gitlab registry, if you don't then build the image manually with file in `https://gitlab.com/altinity-public/container-images/-/tree/main/test/clickhouse-driver-runner` and change the previous command with

```bash
docker run --rm -v $(pwd)/PACKAGES:/clickhouse -e RELEASE=0.2.1 clickhouse-driver
```

Where `clickhouse-driver` is the name of created docker image.

Link to the instruction how to run tests on local machine `https://clickhouse-driver.readthedocs.io/en/latest/development.html`

Patches are needed to make tests work with clickhouse 22.3-lts.
