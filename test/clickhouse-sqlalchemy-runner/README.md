# Running clickhouse-sqlalchemy tests locally

clickhouse-sqlalchemy: `https://github.com/xzkostyan/clickhouse-sqlalchemy/`

To run clickhouse-sqlalchemy tests manually you need `clickhouse-common-static.deb` and the `clickhouse-client.deb` in your machine.

Then execute command
```bash
docker run --rm -v $(pwd)/PACKAGES:/clickhouse -e RELEASE=0.2.0 registry.gitlab.com/altinity-public/container-images/test/clickhouse-sqlalchemy-runner:v1.0
```
Where `$(pwd)/PACKAGES` is the folder to the clickhouse*.deb packages and `RELEASE=0.2.0` is the clickhouse-sqlalchemy version you want to test

Make sure you have access to gitlab registry, if you don't then build the image manually with file in `https://gitlab.com/altinity-public/container-images/-/tree/main/test/clickhouse-sqlalchemy-runner` and change the previous command with
```bash
docker run --rm -v $(pwd)/PACKAGES:/clickhouse -e RELEASE=0.2.0 clickhouse-sqlalchemy
```
Where `clickhouse-driver` is the name of created docker image.

Patch is needed to make tests work.
