#!/bin/bash
set -xe

# install clickhouse, make sure binaries for common and client in /clickhouse folder
dpkg -i clickhouse/clickhouse-common-static*.deb
dpkg -i clickhouse/clickhouse-client*.deb

clickhouse server --daemon
sleep 10
clickhouse-client -q "SELECT 1"

git clone --branch "${RELEASE}" --depth 1 --single-branch "https://github.com/xzkostyan/clickhouse-sqlalchemy.git"
cd clickhouse-sqlalchemy

git apply /clickhouse-sqlalchemy.patch

pip install --upgrade pip
pip install asynch
python3 testsrequire.py

python3 setup.py test
