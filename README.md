# Container Images

This public repository contains only public docker images from Altinity which can be used for CI/CD other public project

Look to container registry https://gitlab.com/altinity-public/container-images/container_registry for list images.

Please follow naming convention for images, example an image named

    registry.gitlab.com/altinity-public/container-images/builder

is expected to have its Dockerfile in path `container-images/builder`

